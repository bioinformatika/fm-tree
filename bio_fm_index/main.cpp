//
//  main.cpp
//  bio_fm_index
//
//  Created by Juraj Pavlek on 12/12/2019.
//  Copyright © 2019 Juraj Pavlek. All rights reserved.
//
//  Big thank you to Felipe Louza!
#include <iostream>
#include <cstring>
#include <string>
#include <vector>
#include <chrono>
#include <cmath>
#include <list>
#include <queue>
#include <tuple>
#include <algorithm>
#include<iostream>
#include<fstream>
#include "sacak-lcp.h"
#include "sacak-lcp.c"




using namespace std;

class CounterNode{
    
    public:
    unsigned  int atIndex;
    unsigned  int A;
    unsigned  int C;
    unsigned  int G;
    unsigned  int T;
    unsigned  int dollar;


    CounterNode(unsigned  index,unsigned  int a,unsigned  int c,unsigned  int g,unsigned  int t,unsigned  int dollarInput){
        atIndex = index;
        A = a;
        C = c;
        G = g;
        T = t;
        dollar = dollarInput;
    }
};

class Node {
    
    public:
  
    unsigned  int ep;
    unsigned  int sp;
    int layer;
    
    
    Node(unsigned  int epInput, unsigned  int spInput, int layerInput) {
        ep = epInput;
        sp = spInput;
        layer = layerInput;
    }
};


class FMTreeCluster {
    
public:
    int counterNodeStretchConstant = 4;
    string fullText;
    unsigned  int c[5];
    unsigned  int charCount[5];
    vector<CounterNode> counterNodeVector;
    char allChars[6];
    vector<unsigned  int> ssa;
    vector<int> b;
    vector<int> bCount;


    string comString;
    
    unsigned  int getBOcc(unsigned  int index) {
        
        unsigned int sum = bCount.at(index / counterNodeStretchConstant);
        
        unsigned int starting = index - (index % counterNodeStretchConstant);
        
        for (unsigned  int i = starting + 1; i <= index; i++) {
            sum += b[i];
        }

        return sum - 1;
    }
    
    void printStarting(unsigned  int index){
        string s;
        for (unsigned  int i = 0; i < 5; i++) {
            cout << comString[(index + i) % comString.size()];
        }
        cout << "\n";
    }
    
    FMTreeCluster(char * inputText) {
        strcpy(allChars, "$ACGT");
        for(int i = 0; i<5; i++){
            c[i] = 0;
            charCount[i] = 0;
        }
        
        unsigned int n = strlen(inputText)+1;
        
        fullText = "";
        
        uint_t *SA = (uint_t *)malloc(n * sizeof(uint_t));
        int_t *LCP = (int_t *)malloc(n * sizeof(int_t));
        
        // sort
        auto start = std::chrono::high_resolution_clock::now();

        
        
        sacak_lcp((unsigned char *)inputText, (uint_t*)SA, LCP, n);
        auto stop = std::chrono::high_resolution_clock::now();
        
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        
        cout << "Duration SACAK: " << duration.count() << endl;
        
        auto start2 = std::chrono::high_resolution_clock::now();


        int bCounter = 0;
        for(unsigned int i = 0; i < n; ++i) {
            char j = (SA[i])? inputText[SA[i]-1]:'$';
            fullText += j;
            if (SA[i] % 4 == 0) {
                bCounter++;
                b.insert(b.end(), 1);
                ssa.insert(ssa.end(), SA[i]);
            } else {
                b.insert(b.end(), 0);
            }

            if(i % counterNodeStretchConstant == 0){
                
                bCount.insert(bCount.end(), bCounter);
                CounterNode node = CounterNode(i,charCount[1], charCount[2], charCount[3], charCount[4], charCount[0]);
                counterNodeVector.insert(counterNodeVector.end(), node);
            }
            
            switch (j) {
                case 'A':
                    charCount[1] += 1;
                    break;
                case 'C' :
                    charCount[2] += 1;
                    break;
                case 'G' :
                    charCount[3] += 1;
                    break;
                case 'T' :
                    charCount[4] += 1;
                    break;
                case '$' :
                    charCount[0] += 1;
                break;
            }

        }

//        fullText = burrowWheelerTransform(inputText);
        
        unsigned  int sum = 0;
        for (int i = 0; i < 6; ++i) {
            c[i] = sum;
            sum += charCount[i];
        }
        
        
        auto stop2 = std::chrono::high_resolution_clock::now();
        
        auto duration2 = std::chrono::duration_cast<std::chrono::microseconds>(stop2 - start2);
        
        cout << "Duration INIT: " << duration2.count() << endl;

    }
    
    
    
    //performs Burrow Wheeler transoformation of a given string and returns
    //the transformed value.
    //More on BWT can be found on: https://en.wikipedia.org/wiki/Burrows–Wheeler_transform
    string burrowWheelerTransform(string compressingString){
        
        vector<tuple<int, char>> charNextCharVector;
        compressingString += "$";
        comString = compressingString;

        for(string::size_type i = 0; i < compressingString.size(); ++i) {
            
            tuple<int, char> charPair = {i, compressingString[i]};
            charNextCharVector.insert(charNextCharVector.end(), charPair);
        }
        unsigned  int size = compressingString.size();

        //sorting by suffixes alphabetically
        sort(charNextCharVector.begin(), charNextCharVector.end(),
             [=](const tuple<int,char>& a, const tuple<int,char>& b) -> bool {
            
            int firstPos = get<0>(a);
            int secondPos = get<0>(b);

            unsigned  int iterations = min(firstPos, secondPos);
            iterations = compressingString.size() - iterations - 1;
            
            for(string::size_type i = 1; i <= iterations; ++i) {
                if (compressingString[(firstPos + i) % size] > compressingString[(secondPos + i) % size]) {
                    return false;
                }
                if (compressingString[(firstPos + i) % size] < compressingString[(secondPos + i) % size]) {
                    return true;
                }
            }
            
            if (firstPos > secondPos) {
                return true;
            }
            return false;
            });
        
        string compressedString;
        
        for(string::size_type i = 0; i < compressingString.size(); ++i) {
            char character = get<1>(charNextCharVector[i]);

            compressedString += character;
            
            if (get<0>(charNextCharVector[i]) % counterNodeStretchConstant == 0) {
                ssa.insert(ssa.end(), get<0>(charNextCharVector[i]));
                b.insert(b.end(), 1);
            } else {
                b.insert(b.end(), 0);
            }
            
            if(i % counterNodeStretchConstant == 0){
                CounterNode node = CounterNode(i,charCount[1], charCount[2], charCount[3], charCount[4], charCount[0]);
                counterNodeVector.insert(counterNodeVector.end(), node);
            }
            
            switch (character) {
                case 'A':
                    charCount[1] += 1;
                    break;
                case 'C' :
                    charCount[2] += 1;
                    break;
                case 'G' :
                    charCount[3] += 1;
                    break;
                case 'T' :
                    charCount[4] += 1;
                    break;
                case '$' :
                    charCount[0] += 1;
                break;
            }
            
        }

        return compressedString;
    }

    vector<int> addIndexesFromSuffix(int sp, int ep) {
        
        vector<int> r;
        
        return r;
    }

    vector<unsigned  int> earlyLeafNodeCalculation(string p, unsigned  int sp1, unsigned  int ep1){
        auto start = std::chrono::high_resolution_clock::now();


        vector<unsigned  int> r = earlyLeafNodePhaseTwo(p,sp1,ep1);
        transform(begin(r),end(r),begin(r),[](int x){return x-1;});
        
        
        auto stop = std::chrono::high_resolution_clock::now();
        
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        return r;
    }
    
    tuple<int, int> backwardsSearch(string p){
        
         int i = p.size() - 1;
        char s = p.at(i);
        
        i = i -1;
        unsigned  int sp = c[getIndexOfChar(s)];
        unsigned  int ep = c[getIndexOfChar(s) + 1] - 1;
        
        
        while((i >= 0) && (sp <= ep)){
            s = p.at(i);
            i = i - 1 ;
            
            unsigned  int epNext = ep + 1;
               
            sp = c[getIndexOfChar(s)] + getOcc(s,sp);
            ep = c[getIndexOfChar(s)] + getOcc(s,epNext) - 1;
            
        }
        if(ep < sp){
            cout << "Uzorak ne postoji\n";
        } else {
            tuple<int,int> returnTuple = {sp, ep};
            return returnTuple;
        }
        return {sp,ep};
    }

    vector<unsigned  int> locateSA(unsigned  int sp,unsigned  int ep, int max){
        vector<unsigned  int> r;

        for( int i = sp; i <= ep; i++){
            int j = i;
            int m = 0;
            bool maxReached = false;
            while(b[j] == 0){
                if(m == max){
                    maxReached = true;
                    break;
                }
                j = LF(j, fullText[j]);
                m++;
            }
            unsigned int entry = ssa.at(getBOcc(j)) + m;
            if(!maxReached){
                r.insert(r.end(), entry);
            }
        }
        return r;
        
    }

    unsigned  int LF(unsigned  int i, char ch){
        unsigned  int lf = c[getIndexOfChar(ch)] + getOcc(ch,i);
        return lf;
    }

    vector<unsigned  int> earlyLeafNodePhaseTwo(string p, unsigned  int sp, unsigned  int ep){
        char first = p.at(0);
        
        vector<unsigned  int> r;
        for(unsigned  int i = sp; i <= ep ; i++){
            if(b[i] == 1){
//                unsigned  int x = ssa[getBOcc(i)];
                
                if(fullText.at(i) == first){
                    r.insert(r.end(), ssa[getBOcc(i)]);
                }
            }
        }
        return r;
    }

    // Returns number of occurences of a character ( c ) before a given index from the BWT transformation of the full text
    unsigned  int getOcc(char c,unsigned  int i){

        
        unsigned  int index = i - 1;
//        cout << index << " -->INDEX \n";
        CounterNode cn = counterNodeVector[index/counterNodeStretchConstant];
        
        unsigned  int fromNode;
        switch(c){
            case 'A':
                fromNode = cn.A;
                break;
            case 'C':
                fromNode = cn.C;
                break;
            case 'G':
                fromNode = cn.G;
                break;
            case 'T':
                fromNode = cn.T;
                break;
            default:
                fromNode = 0;
        }

        for(unsigned  int x = cn.atIndex; x <= index; x++){
            if(fullText.at(x) == c){
                fromNode++;
            }
        }
        
        return fromNode;
    }

    int getIndexOfChar(char c){
        for(int x = 0; x < strlen(allChars);x++){
            if(c == allChars[x]){
                return x;
            }
        }
        
        return -1;
    }

    ifstream readFileToStream(string path){
        ifstream dnaFile;
        dnaFile.open(path);
        return dnaFile;
    }

    vector<unsigned  int> treeAlgorithm(string p, unsigned  int sp, unsigned  int ep, unsigned  int spSuffix, unsigned  int epSuffix, int threshold){
        
        int d = counterNodeStretchConstant;
        
        cout << "D:" << d << "\n";
        
        
        unsigned  int total_num = ep - sp + 1;
        vector<unsigned  int> r = earlyLeafNodeCalculation(p,spSuffix, epSuffix);
        unsigned int num = r.size();
        Node node = Node(ep, sp, 0);
        int tree_height = d - 1;
        queue<Node> nodeQueue;
        nodeQueue.push(node);
        
        while (!nodeQueue.empty() && num < total_num) {
            Node &currentNode = nodeQueue.front();
            nodeQueue.pop();
            
            if (currentNode.ep - currentNode.sp + 1 < threshold) {
                
                vector<unsigned  int> calculatedPositions = locateSA(currentNode.sp, currentNode.ep,tree_height - currentNode.layer -1);
                num += calculatedPositions.size();

                for(unsigned int i = 0; i < calculatedPositions.size(); i++){
                    int newPosition = calculatedPositions[i];
                    if(find(r.begin(), r.end(), newPosition) == r.end()) {
                        r.insert(r.end(), newPosition);
                    }
                }
                
            } else {
                
                for (unsigned  int z = currentNode.sp; z <= currentNode.ep; z++) {
                    if (b[z] == 1) {
                        unsigned  newPosition =ssa[getBOcc(z)] + currentNode.layer ;
                        if(find(r.begin(), r.end(), newPosition) == r.end()) {
                            r.insert(r.end(), newPosition);
                        }
                       
                        
                        num++;
                    }
                }
                
                if (currentNode.layer + 1 < d - 1) {
                    
                    for (int i = 0; i < 4; ++i) {
                        unsigned  int epInput = c[i+1] + getOcc(allChars[i+1], currentNode.ep + 1) - 1;
                        unsigned  int spInput = c[i+1] + getOcc(allChars[i+1], currentNode.sp);
                        
                        Node childNode = Node(epInput, spInput, currentNode.layer + 1);
                        nodeQueue.push(childNode);
                    }
                }
            }
        }
        return r;
    }
};

//returns a string of a given number's binary representation
//without the first 1
//(eg. 5 will return "001")
string decToBinary(int n) {
    string binaryString;
    int i = 0;
    while (n > 1) {
        binaryString = to_string(n%2) + binaryString;
        n = n / 2;
        i++;
    }
    return binaryString;
}

//compresses string to a format where each letter next to
//itself has a binary representation of how many times
//it consecutively repeats
//(eg. "CCCGGGGTAA" will be compressed to "C01G00TA0")
string compressBinary(string compressingString) {
    string compressedString;
    int count = 1;
    compressedString += compressingString[0];
    for(string::size_type i = 1; i < compressingString.size(); ++i) {
        if (compressingString[i] == compressedString.back()) {
            count++;
        } else {
            compressedString += decToBinary(count);
            count = 1;
            compressedString += compressingString[i];
        }
    }
    compressedString += decToBinary(count);
    
    return compressedString;
}

void print(vector<unsigned  int> const &input) {
    for (int i = 0; i < input.size(); i++) {
        cout << input.at(i) << " ";
    }
}

void print(vector<CounterNode> const &input) {
    for (int i = 0; i < input.size(); i++) {
        cout << "A" << input.at(i).A << "\n";
        cout << "C" << input.at(i).C << "\n";
        cout << "G" << input.at(i).G << "\n";
        cout << "T" << input.at(i).T << "\n";
        cout << "index" << input.at(i).atIndex << "\n";
        cout << "\n";
    }
}

void print(vector<int> const &input) {
    for (int i = 0; i < input.size(); i++) {
        cout << input.at(i) << " ";
    }
}
string readFromFileToArray(string path){
    ifstream in(path);
    string contents((istreambuf_iterator<char>(in)), istreambuf_iterator<char>());
    return contents;
}

char * redFromFileToCharPointer(string path){
    const char * cstr = path.c_str();
    FILE * f = fopen(cstr,"rb");
    fseek(f, 0, SEEK_END);
    int fsize = ftell(f);
    fseek (f, 0, SEEK_SET);

    char * stringulja = (char *)malloc(fsize + 1);
    fread(stringulja, fsize, 1, f);
    fclose(f);
    return stringulja;
}

int main(int argc, const char * argv[]) {
    string filePath = argv[1];

    char * myString = redFromFileToCharPointer(filePath);
    

    FMTreeCluster tree = FMTreeCluster(myString);
    
    
    while(true) {
        string pattern;
        cout << "Search pattern: \n";
        cin >> pattern;
        if (pattern == "") {
            return 0;
        }
        string patternSuffix = pattern.substr(1, pattern.size() - 1);
        auto start = std::chrono::high_resolution_clock::now();

        tuple<unsigned  int, unsigned  int> spEpTuple = tree.backwardsSearch(pattern);

        tuple<unsigned  int, unsigned  int> spEpSuffixTuple = tree.backwardsSearch(patternSuffix);
        // cout << "Compressed string: \n" << tree.fullText << "\n\n";

        vector<unsigned  int> positions = tree.treeAlgorithm(pattern, get<0>(spEpTuple), get<1>(spEpTuple), get<0>(spEpSuffixTuple), get<1>(spEpSuffixTuple), 4);
        auto stop = std::chrono::high_resolution_clock::now();

    //    cout << "Positions :";
    //    print(positions);
        cout << endl;
        cout << "Position count " << positions.size() << endl;


        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        cout << "Duration Tree: " << duration.count() << endl;
    }
    
    
    
    return 0;
}
